# Installing Jenkins on a Server 2024

### Pre-Requisites
Digital Ocean Droplet

Docker

#### Project Outline

There are two ways to install Jenkins on a server. We can download as a package and install on a server, or run Jenkins as a Docker container.

In this project we will run Jenkins as a Docker container.

#### Getting started

Lets go to our digital ocean account

![Image 1](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image1.png)

Create the droplet

![Image 2](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image2.png)

![Image 3](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image3.png)

Now lets modify the firewall

![Image 4](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image4.png)

Now lets ssh into the server

![Image 5](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image5.png)


Now ready to install docker on the server

```
apt install docker.io
```

![Image 7](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image7.png)

![Image 8](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image8.png)

Now ready to run Jenkins as a container using Jenkins image on docker hub and specificying port 8080 for container access and 50000 for communication between the instances

```
docker run -p 8080:8080 -p 50000:50000 -d -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts
```

![Image 9](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image9.png)

```
docker ps
```

![Image 10](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image10.png)

Can now access on the server via IP address and port

```
<droplet_ip>:<container_port>
```

![Image 11](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image11.png)

Now checking the initialPassword

```
docker exec -it 659cfc6fbc3f bash
```

![Image 12](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image12.png)

```
cat /var/jenkins_home/secrets/initialAdminPassword
```

![Image 13](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image13.png)

Loggin in

![Image 18](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image18.png)

![Image 19](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image19.png)

Can installation is now complete

![Image 20](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image20.png)

![Image 21](https://gitlab.com/FM1995/installing-jenkins-on-a-server-2024/-/raw/main/Images/Image21.png)





